var through = require("through");
var gutil = require("gulp-util");
var request = require("request");
var col = gutil.colors;

module.exports = function(urls){
	var stream = through(function(file,enc,cb){
		this.push(file);
		cb();
	});


	var files = typeof urls === 'string' ? [urls] : urls;
	var downloadCount = 0;

	function download(url){
		var firstLog = true;
    var fs = require('fs');
    fs.readFile('./cookies.txt', function (err, data) {
		request({url:url,encoding:null,jar:true, headers:{
			'User-Agent': 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
    'Cookie': data,
    'Accept': '*/*',
    'Connection': 'keep-alive'
     }},downloadHandler);
	});
		function downloadHandler(err, res, body){
			if(err){
				console.log(err);
			}else{
			var fileName = url.split('/').pop();
			var file = new gutil.File( {path:fileName, contents: new Buffer(body)} );
			stream.queue(file);
			process.stdout.write('['+col.green('gulp')+']'+' Downloading '+col.cyan(url)+'...');
			process.stdout.write(' '+col.green('Done\n'));
			downloadCount++;
			if(downloadCount != files.length){
				download(files[downloadCount])
			}else{
				stream.emit('end')
			}
		}
		}
	}
	download(files[0])

	return stream;
}

