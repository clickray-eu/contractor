$(document).ready(function(){

    var header = $('.header.header');
    var mainMenu = $('.main-menu');

$(window).on('scroll', function(){

if( $(window).scrollTop() > 10 ){
header.addClass('shadow');
}else{
    header.removeClass('shadow');
}

})
if($(window).width()<768){
    mainMenu.hide();
}else{
    mainMenu.show();
}
$(window).resize(function(){
    if($(window).width()<768){
        mainMenu.hide();
    }else{
        mainMenu.show();
    }
})

header.find('>div').append('<div class="hamburger"><div></div><div></div><div></div></div>');
header.find('.hamburger').on('click', function(){
$(this).toggleClass('active');
mainMenu.toggle(300);
})

});