$(document).ready(function () {
    var bgAndContent = $('.bg-and-content');
    var bgTriangle = $('.bg-triangle');
    var bgTriangleTwo = $('.bg-triangle-two');
    var svg = $('.svg-container')
    var height = 0;
    setTimeout(function() {
        height = bgAndContent.find('.content').innerHeight();
        bgAndContent.find('.bg-triangles').css({ "height": height + 1 });
        bgAndContent.find('.white-mask-ie-repair').css({ "height": height + 1 });
   }, 400);
    var setHeight = function () {
        height = bgAndContent.find('.content').innerHeight();
        bgAndContent.find('.bg-triangles').css({ "height": height + 1 });
        bgAndContent.find('.white-mask-ie-repair').css({ "height": height + 1 });
    }
    setHeight();
    $(window).resize(function () {
        if (screen.width < 1100) {
            setHeight();
        }
    });
    if($('body').hasClass('ms-browser')){
        bgAndContent.find('.white-mask-ie-repair').addClass('turn-on');
        bgTriangle.find('.white-mask-ie-repair-2').addClass('turn-on');
        bgAndContent.find('.text--color--white--991').removeClass('text--color--white--991');
        bgTriangleTwo.css({"display":"none"});
        svg.css({"display":"block"});
    }
});