$(document).ready(function () {
    var mySwiper = new Swiper ('.swiper-container', {
      loop: true,
      slidesPerView: 5,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      breakpoints: {
        1200: {
          slidesPerView: 4,
        },
        768: {
          slidesPerView: 3,
        },
        576: {
          slidesPerView: 2,
        },
        400: {
          slidesPerView: 1,
        },
      }
    })
  });