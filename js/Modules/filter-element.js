$(document).ready(function () {

    var items = $('.filter-element-items');
    var buttons = $('.filter-element-buttons');

    buttons.find('.element').click(function(){
        buttons.find('.element').removeClass('active');
        $(this).addClass('active');
    });

    buttons.find('.element:nth-child(1)').click();
    // $('.filter-element-items').isotope({
    //     // options
    //     itemSelector: '.filtered-item',
    //     layoutMode: 'fitRows'
    //   });


    var $grid = items.isotope({
        // options
    });
    // filter items on button click
    buttons.on('click', '.element', function () {
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({ filter: filterValue });
    });


    buttons.find('.element').hover(function () {
        $(this).addClass('active-two');
    }, function () {
        $(this).removeClass('active-two');
    });


});