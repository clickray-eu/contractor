$(document).ready(function () {
    var mySwiper = new Swiper ('.clients-opinion-slider', {
      loop: true,
      slidesPerView: 3,
      // autoplay: {
      //   delay: 2500,
      //   disableOnInteraction: false,
      // },
      breakpoints: {
        991: {
          slidesPerView: 1,
        },
      },
    pagination: {
        el: '.swiper-pagination',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    })
  });