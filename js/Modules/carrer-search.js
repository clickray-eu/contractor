
if ($('.me-career-search-banner__search-form').length) {
    var requirements = 0 ;
    var searchInput = $("#careerSearchInput");
    var searchInput2 = $("#careerSearchInput2");
    var job_title = "";
    var jsonJobs=[];
  
    $('.me-job-offer-row').each(function() {
      var job_title = $(this).find('.me-job-offer-row__job-title').html();
      var job_keywords = $(this).find('.me-job-offer-row__keywords').html();
      var job_location = $(this).find('.me-job-offer-row__location').html();
      var id = $(this);
      jsonJobs.push({'id':id ,'title':job_title, 'keywords':job_keywords, 'locations': job_location });
    });
  }
  
  $('.me-career-search-banner__search-form form input[type=search]').on('input', function(e) {
    jobOfferSearch();
  });
  
  $('.me-career-search-banner__search-form form').on('submit', function(e) {
    jobOfferSearch();
  });
  
  $('.me-career-search-banner__search-form form input[type=submit]').on('click', function(e) {
    jobOfferSearch();
  
    $('html, body').animate({
      scrollTop: $('.me-job-offer-row').first().parent().parent().offset().top - 130
    }, 900);
  });
  
  function jobOfferSearch() {
    // $('.me-job-offer-row').removeClass('me-job-offer-row--hide');
  
    var job = searchInput.val();
    var location = searchInput2.val();
    requirements = 0;
    if(job != "") {
      requirements++ ;
    }
  
    if(location != "") {
      requirements++;
    }
  
    for(var i = 0; i<jsonJobs.length; i++) {
      let req = requirements;
  
      if(job != null && job != "") {
  
        if(job.search(" ") > -1) {
          var splitted_text = job.toLocaleLowerCase().split(" ");
          var count = 0;
          var templateJob = jsonJobs[i].title + jsonJobs[i].keywords;
          for(var j=0; j<splitted_text.length; j++) {
            if(templateJob.toLocaleLowerCase().search(splitted_text[j]) > -1) {
              count++;
            }
          }
  
          if(count == splitted_text.length) {
            req--;
          }
        } else {
          var templateJob = jsonJobs[i].title + jsonJobs[i].keywords;
          if(templateJob.toLocaleLowerCase().search(job.toLocaleLowerCase()) > -1) {
            req--;
          }
        }
      }
  
      if(location != null && location != "") {
  
        if(location.search(" ") > -1) {
          var splitted_text = location.toLocaleLowerCase().split(" ");
          var count = 0;
          var templateJob = jsonJobs[i].locations;
          for(var j=0; j<splitted_text.length; j++) {
            if(templateJob.toLocaleLowerCase().search(splitted_text[j]) > -1) {
              count++;
            }
          }
  
          if(count == splitted_text.length) {
            req--;
          }
        } else {
          var templateJob = jsonJobs[i].locations;
          if(templateJob.toLocaleLowerCase().search(location.toLocaleLowerCase()) > -1) {
            req--;
          }
        }
      }
  
      if(req > 0) {
        jsonJobs[i].id.fadeOut();
      } else {
        jsonJobs[i].id.fadeIn();
      }
    }
  }