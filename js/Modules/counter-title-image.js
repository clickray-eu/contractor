$(document).ready(function () {
if( $('.counter-title-image-wrapper').hasClass('counter-working') ){

    var counter = $(document).find('.counter-title-image-wrapper');
    var counterPosition = 0;
    var flag = true;
    var counterStart = function () {
        $(counter).find('.numbers-counter').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                    duration: 3000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
        });
    }
    var setCounterPosition = function () {
        counterPosition = $(counter).offset();
    }
    setTimeout(setCounterPosition, 500);
    $(document).on('scroll', function () {
        if (($(window).scrollTop() + $(window).innerHeight()) > (counterPosition.top + 200)) {
            if (flag == true) {
                counterStart();
            }
            flag = false;
        }
    })
}

});
