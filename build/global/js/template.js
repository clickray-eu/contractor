'use strict';

// file: detect-browser.js
$(document).ready(function () {
  iOS();
  if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
    $('body').addClass('ms-browser');
  }

  if (!!window.MSInputMethodContext && !!document.documentMode) {
    $('body').addClass('ie');
  }
});

function iOS() {
  var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  if (iOS) $('body').addClass('ios');
}

if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
  $('body').addClass('ms-browser');
}

if (!!window.MSInputMethodContext && !!document.documentMode) {
  $('body').addClass('ie');
}

if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
  $('body').addClass('ff');
}

if (navigator.userAgent.toLowerCase().indexOf('safari/') > -1 && navigator.userAgent.toLowerCase().indexOf('chrome') < 1) {
  $('body').addClass('safari');
}

if ("ontouchstart" in document.documentElement) {
  $('body').addClass('touch-device');
}
// end file: detect-browser.js

// file: form.js
$(document).ready(function () {
  setTimeout(function () {
    $('.transparent-two-column-form form .hs_file.hs-file .input .hs-input').addClass('bfi');
    bfi_init();
    $('.transparent-two-column-form form .hs_file.hs-file .input .bfi-container .bfi-label').html('<p>Choose or drop one file</p> <span> Choose file </span>');
  }, 500);
});

// end file: form.js

// file: header.js
$(document).ready(function () {

  var header = $('.header.header');
  var mainMenu = $('.main-menu');

  $(window).on('scroll', function () {

    if ($(window).scrollTop() > 10) {
      header.addClass('shadow');
    } else {
      header.removeClass('shadow');
    }
  });
  if ($(window).width() < 768) {
    mainMenu.hide();
  } else {
    mainMenu.show();
  }
  $(window).resize(function () {
    if ($(window).width() < 768) {
      mainMenu.hide();
    } else {
      mainMenu.show();
    }
  });

  header.find('>div').append('<div class="hamburger"><div></div><div></div><div></div></div>');
  header.find('.hamburger').on('click', function () {
    $(this).toggleClass('active');
    mainMenu.toggle(300);
  });
});
// end file: header.js

// file: wait-for-load.js
function waitForLoad(wrapper, element, callback) {
  if ($(wrapper).length > 0) {
    $(wrapper).each(function (i, el) {
      var waitForLoad = setInterval(function () {
        if ($(el).length == $(el).find(element).length) {
          clearInterval(waitForLoad);
          callback($(el), $(el).find(element));
        }
      }, 50);
    });
  }
}
// end file: wait-for-load.js

// file: Modules/carrer-search.js

if ($('.me-career-search-banner__search-form').length) {
  var requirements = 0;
  var searchInput = $("#careerSearchInput");
  var searchInput2 = $("#careerSearchInput2");
  var job_title = "";
  var jsonJobs = [];

  $('.me-job-offer-row').each(function () {
    var job_title = $(this).find('.me-job-offer-row__job-title').html();
    var job_keywords = $(this).find('.me-job-offer-row__keywords').html();
    var job_location = $(this).find('.me-job-offer-row__location').html();
    var id = $(this);
    jsonJobs.push({ 'id': id, 'title': job_title, 'keywords': job_keywords, 'locations': job_location });
  });
}

$('.me-career-search-banner__search-form form input[type=search]').on('input', function (e) {
  jobOfferSearch();
});

$('.me-career-search-banner__search-form form').on('submit', function (e) {
  jobOfferSearch();
});

$('.me-career-search-banner__search-form form input[type=submit]').on('click', function (e) {
  jobOfferSearch();

  $('html, body').animate({
    scrollTop: $('.me-job-offer-row').first().parent().parent().offset().top - 130
  }, 900);
});

function jobOfferSearch() {
  // $('.me-job-offer-row').removeClass('me-job-offer-row--hide');

  var job = searchInput.val();
  var location = searchInput2.val();
  requirements = 0;
  if (job != "") {
    requirements++;
  }

  if (location != "") {
    requirements++;
  }

  for (var i = 0; i < jsonJobs.length; i++) {
    var req = requirements;

    if (job != null && job != "") {

      if (job.search(" ") > -1) {
        var splitted_text = job.toLocaleLowerCase().split(" ");
        var count = 0;
        var templateJob = jsonJobs[i].title + jsonJobs[i].keywords;
        for (var j = 0; j < splitted_text.length; j++) {
          if (templateJob.toLocaleLowerCase().search(splitted_text[j]) > -1) {
            count++;
          }
        }

        if (count == splitted_text.length) {
          req--;
        }
      } else {
        var templateJob = jsonJobs[i].title + jsonJobs[i].keywords;
        if (templateJob.toLocaleLowerCase().search(job.toLocaleLowerCase()) > -1) {
          req--;
        }
      }
    }

    if (location != null && location != "") {

      if (location.search(" ") > -1) {
        var splitted_text = location.toLocaleLowerCase().split(" ");
        var count = 0;
        var templateJob = jsonJobs[i].locations;
        for (var j = 0; j < splitted_text.length; j++) {
          if (templateJob.toLocaleLowerCase().search(splitted_text[j]) > -1) {
            count++;
          }
        }

        if (count == splitted_text.length) {
          req--;
        }
      } else {
        var templateJob = jsonJobs[i].locations;
        if (templateJob.toLocaleLowerCase().search(location.toLocaleLowerCase()) > -1) {
          req--;
        }
      }
    }

    if (req > 0) {
      jsonJobs[i].id.fadeOut();
    } else {
      jsonJobs[i].id.fadeIn();
    }
  }
}
// end file: Modules/carrer-search.js

// file: Modules/clients-opinion.js
$(document).ready(function () {
  var mySwiper = new Swiper('.clients-opinion-slider', {
    loop: true,
    slidesPerView: 3,
    // autoplay: {
    //   delay: 2500,
    //   disableOnInteraction: false,
    // },
    breakpoints: {
      991: {
        slidesPerView: 1
      }
    },
    pagination: {
      el: '.swiper-pagination'
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    }
  });
});
// end file: Modules/clients-opinion.js

// file: Modules/counter-title-image.js
$(document).ready(function () {
  if ($('.counter-title-image-wrapper').hasClass('counter-working')) {

    var counter = $(document).find('.counter-title-image-wrapper');
    var counterPosition = 0;
    var flag = true;
    var counterStart = function counterStart() {
      $(counter).find('.numbers-counter').each(function () {
        $(this).prop('Counter', 0).animate({
          Counter: $(this).text()
        }, {
          duration: 3000,
          easing: 'swing',
          step: function step(now) {
            $(this).text(Math.ceil(now));
          }
        });
      });
    };
    var setCounterPosition = function setCounterPosition() {
      counterPosition = $(counter).offset();
    };
    setTimeout(setCounterPosition, 500);
    $(document).on('scroll', function () {
      if ($(window).scrollTop() + $(window).innerHeight() > counterPosition.top + 200) {
        if (flag == true) {
          counterStart();
        }
        flag = false;
      }
    });
  }
});

// end file: Modules/counter-title-image.js

// file: Modules/filter-element.js
$(document).ready(function () {

  var items = $('.filter-element-items');
  var buttons = $('.filter-element-buttons');

  buttons.find('.element').click(function () {
    buttons.find('.element').removeClass('active');
    $(this).addClass('active');
  });

  buttons.find('.element:nth-child(1)').click();
  // $('.filter-element-items').isotope({
  //     // options
  //     itemSelector: '.filtered-item',
  //     layoutMode: 'fitRows'
  //   });


  var $grid = items.isotope({
    // options
  });
  // filter items on button click
  buttons.on('click', '.element', function () {
    var filterValue = $(this).attr('data-filter');
    $grid.isotope({ filter: filterValue });
  });

  buttons.find('.element').hover(function () {
    $(this).addClass('active-two');
  }, function () {
    $(this).removeClass('active-two');
  });
});
// end file: Modules/filter-element.js

// file: Modules/image-slider.js
$(document).ready(function () {
  var mySwiper = new Swiper('.swiper-container', {
    loop: true,
    slidesPerView: 5,
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    },
    breakpoints: {
      1200: {
        slidesPerView: 4
      },
      768: {
        slidesPerView: 3
      },
      576: {
        slidesPerView: 2
      },
      400: {
        slidesPerView: 1
      }
    }
  });
});
// end file: Modules/image-slider.js

// file: Modules/pagination-job-offer.js
$(document).ready(function () {

  // $('.pagination-container').pagination({
  //     dataSource: [1, 2, 3, 4, 5, 6, 7],
  //     pageSize: 5,
  //     callback: function(data, pagination) {
  //         // template method of yourself
  //         var html = template(data);
  //         $('.data-container >span').html(html);
  //     }
  // })

  // $('.data-container >span').pagination({
  //     dataSource: ['elo', 'no siema', 'jestem', 4, 5, 6, 7, 100],
  //     pageSize: 5,
  //     showPrevious: false,
  //     showNext: false,
  //     callback: function(data, pagination) {
  //         // template method of yourself
  //         var html = template(data);
  //         dataContainer.html(html);
  //     }
  // })

});
// end file: Modules/pagination-job-offer.js

// file: Website/backgrounds.js
$(document).ready(function () {
  var bgAndContent = $('.bg-and-content');
  var bgTriangle = $('.bg-triangle');
  var bgTriangleTwo = $('.bg-triangle-two');
  var svg = $('.svg-container');
  var height = 0;
  setTimeout(function () {
    height = bgAndContent.find('.content').innerHeight();
    bgAndContent.find('.bg-triangles').css({ "height": height + 1 });
    bgAndContent.find('.white-mask-ie-repair').css({ "height": height + 1 });
  }, 400);
  var setHeight = function setHeight() {
    height = bgAndContent.find('.content').innerHeight();
    bgAndContent.find('.bg-triangles').css({ "height": height + 1 });
    bgAndContent.find('.white-mask-ie-repair').css({ "height": height + 1 });
  };
  setHeight();
  $(window).resize(function () {
    if (screen.width < 1100) {
      setHeight();
    }
  });
  if ($('body').hasClass('ms-browser')) {
    bgAndContent.find('.white-mask-ie-repair').addClass('turn-on');
    bgTriangle.find('.white-mask-ie-repair-2').addClass('turn-on');
    bgAndContent.find('.text--color--white--991').removeClass('text--color--white--991');
    bgTriangleTwo.css({ "display": "none" });
    svg.css({ "display": "block" });
  }
});
// end file: Website/backgrounds.js
//# sourceMappingURL=template.js.map
